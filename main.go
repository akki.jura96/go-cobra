/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package main

import "gitlab.com/akki.jura96/go-cobra/cmd"

func main() {
	cmd.Execute()
}
